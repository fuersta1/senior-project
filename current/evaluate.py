import os
from PIL import Image
from cntk.ops.functions import load_model
from time import sleep, time
import numpy as np
import xml
from VideoCapture import Device

cam = Device()
names = os.listdir(os.path.join("..", "pictures"))
model_file = os.path.join("..", "facialRecognition.dnn")
my_network = load_model(model_file)

test_photo = os.path.join("..", "testing", "archtom16.jpg")
tests = os.path.join("..", "testing")
mean_image_path = os.path.join("..", "train_mean.xml")

image_size = 100
mean_image = xml.etree.ElementTree.parse(mean_image_path).getroot()
mean_image = [float(i) for i in mean_image.find('MeanImg').find('data').text.strip().split(' ')]
mean_image = np.array(mean_image, dtype=np.float32).reshape((image_size, image_size, 3)).transpose((2, 0, 1))


def eval_picture(pict):
    pict = pict.resize((100, 100))
    rgb_image = np.asarray(pict, dtype=np.float32)
    bgr_image = rgb_image[..., [2, 1, 0]]
    pic = np.ascontiguousarray(np.rollaxis(bgr_image, 2))
    pic = pic - mean_image
    predictions = np.squeeze(my_network.eval({my_network.arguments[0]: [pic]}))
    top_class = np.argmax(predictions)
    return top_class, predictions[top_class]


def take_pic():
    pic = cam.getImage()
    # return pic
    return Image.open(r"../live_michael.jpg")


def get_name(pic_class):
    return names[pic_class]


def get_sub_pics(image):
    images = []
    top = 0
    left = 0
    right = image_size * 3
    bottom = image_size * 3
    # extract each 200 x 200 pixel from it at 50 pixel intervals
    while bottom < image.size[1]:
        while right < image.size[0]:
            #  The crop rectangle, as a (left, upper, right, lower)-tuple.
            edit = image.crop((left, top, right, bottom))
            edit = edit.resize((image_size, image_size))
            images.append(edit)
            right += 25
            left += 25
        right = image_size
        left = 0
        bottom += 25
        top += 25

    top = image.size[0] // 10
    left = image.size[1] // 10
    right = image.size[0] - left
    bottom = image.size[1] - top
    # try to zoom in on center of image
    while top < bottom and left < right:
        print((left, top, right, bottom))
        edit = image.crop((left, top, right, bottom))
        edit = edit.resize((image_size, image_size))
        images.append(edit)
        top = top + (image.size[0] // 10)
        left = left + (2 * (image.size[1] // 10))
        right = right - (2 * (image.size[0] // 10))
        bottom = bottom - (image.size[1] // 10)
        # edit.show()
    return images


if __name__ == "__main__":
    start = time()
    live_pic = take_pic()
    live_pic.save(r"../live_michael.jpg")
    live_pic = live_pic.resize((640, 480))
    # live_pic = live_pic.rotate(270)
    # live_pic.show()
    pics = get_sub_pics(live_pic)
    best = 0
    print(len(pics))
    best_class = -1
    best_pic = None
    values = []
    for picture in pics:
        # picture.show()
        value = eval_picture(picture)
        values.append(value)
        if best < value[1] < 15:
            best = value[1]
            best_class = value[0]
            best_pic = picture
    best_pic.show()
    print(values)
    print("Elapsed time: {}".format(time() - start))
    print("Best Image: {}".format(best))
    print("Image was {}".format(names[best_class]))
    sleep(100)
