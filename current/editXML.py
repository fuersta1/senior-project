import os


def fix_xml():

    with open(os.path.join("..", "train_mean.xml"), 'r') as xml_file:
        text = xml_file.readlines()

    with open(os.path.join("..", "train_mean.xml"), 'w') as xml_file:
        save_text = ""
        for line in text:
            if "<data>" in line:
                edit_stuff = ""
                line_arr = line.split(" ")
                count = 0
                for number in line_arr:
                    if "e" in number:
                        count += 1
                        edit_stuff += number + " "
                        if count > 4:
                            count = 0
                            edit_stuff += "\n"
                if edit_stuff[len(edit_stuff)-1] == "\n":
                    edit_stuff = edit_stuff[:-1]
                save_text += edit_stuff
            else:
                save_text += line
        xml_file.write(save_text)

    print("XML editing complete")


if __name__ == "__main__":
    pass
