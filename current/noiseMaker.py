from PIL import Image
import os
import random

prob = .01


def black_noise(pixel):
    global prob
    if random.random() < prob:
        return 0, 0, 0
    return pixel


def color_noise(pixel):
    global prob
    if random.random() < prob:
        return random.random() * 256, random.random() * 256, random.random() * 256
    return pixel


# takes a path to an image, opens and adds noise and returns the open image
def add_black_noise(image, probability=.01):
    global prob
    prob = probability
    open_image = Image.open(os.path.join(image))
    open_image = Image.eval(open_image, black_noise)
    return open_image


# takes a path to an image, opens and adds noise and returns the open image
def add_color_noise(image, probability=.01):
    global prob
    prob = probability
    open_image = Image.open(os.path.join(image))
    open_image = Image.eval(open_image, color_noise)
    return open_image


# adds noise to all the images in the testing directory and saves them
def do_noise_thing(probability=.01):
    # chance a pixel will be edited
    global prob
    prob = probability
    image_size = 100

    testing_dir = os.path.join("..", "testing")

    for image in os.listdir(testing_dir):
        open_image = Image.open(os.path.join(testing_dir, image))
        for i in range(image_size):
            for q in range(image_size):
                open_image.putpixel((i, q), black_noise(open_image.getpixel((i, q))))
        print(image, " edited")
        open_image.save(os.path.join(testing_dir, image))

if __name__ == "__main__":
    do_noise_thing()
