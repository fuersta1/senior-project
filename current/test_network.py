import train_test_network
import cntk as c
from cntk.cntk_py import Function
from cntk import Trainer, learning_rate_schedule, UnitType
from cntk.learner import sgd
import os

# script constants
mean_file = os.path.join("..", "train_mean.xml")
training_file = os.path.join("..", "training.txt")
testing_file = os.path.join("..", "testing.txt")
model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "models")
model_name = "peopleMatcher"

input_dim_x = 200
input_dim_y = 200
# input_dim = input_dim_x * input_dim_y
input_dim_channels = 3
output_classes = len(os.listdir(os.path.join("..", "pictures")))
num_hidden_layers = 15
hidden_layer_dim = 1000

model_file = os.path.join("..", "facialRecognition.dnn")
network = Function.load_model(model_file)
reader_test = train_test_network.create_reader(testing_file, False, output_classes)

# print(network.name())
# print("")
#
# print(network.output())
# print(network.outputs())
# print(network.arguments())
# print(network.name)
# print("")

# for parameter in network.parameters():
#     print(parameter)

# for index in range(len(network.outputs())):
#     print("Index {} for output: {}.".format(index, network.outputs()[index].name()))
#     print("Index {} for output: {}.".format(index, network.outputs()[index]))
#     print("Index {} for output: {}.".format(index, type(network.outputs()[index])))

network_output = network.output()
label = c.input_variable(output_classes, name="output_layer")
input_layer = network.arguments()[0]

loss = c.ops.cross_entropy_with_softmax(network_output, label)
label_error = c.ops.classification_error(network_output, label)

learning_rate = .2
learner_schedule = learning_rate_schedule(learning_rate, UnitType.minibatch)
learner = sgd(network.parameters(), learner_schedule)

trainer = Trainer(network_output, (loss, label_error), [learner])

input_map = {
    label: reader_test.streams.labels,
    input_layer: reader_test.streams.features
}

# process minibatches and evaluate the model
metric_numer = 0
metric_denom = 0
minibatch_index = 0

# testing network
epoch_size = sum(1 for _ in open(testing_file))
minibatch_size = 16
sample_count = 0
print("")
while sample_count < epoch_size:
    curr_batch = min(minibatch_size, epoch_size - sample_count)
    data = reader_test.next_minibatch(curr_batch, input_map=input_map)
    trainer.test_minibatch(data)

    # minibatch data to be trained with
    metric_numer += trainer.test_minibatch(data) * curr_batch
    metric_denom += curr_batch

    # Keep track of the number of samples processed so far.
    sample_count += data[input_layer].num_samples
    minibatch_index += 1

performance = (metric_numer * 100.0) / metric_denom
if performance >= 45.0:
    raise ValueError("Testing results too low for use, errs = {:0.1f}%".format(performance))
print("Testing results acceptable for use, errs = {:0.1f}%".format(performance))
