import os
from PIL import Image
import numpy as np
import xml.etree.cElementTree as cET
import xml.dom.minidom
print(os.getcwd())
input_dim_channels = 3
names = os.listdir(os.path.join("..", "pictures"))
mean_file = os.path.join("..", "mean.txt")


def save_mean(fname, data):
    root = cET.Element('opencv_storage')
    cET.SubElement(root, 'Channel').text = str(input_dim_channels)
    cET.SubElement(root, 'Row').text = str(imgSize)
    cET.SubElement(root, 'Col').text = str(imgSize)
    mean_img = cET.SubElement(root, 'MeanImg', type_id='opencv-matrix')
    cET.SubElement(mean_img, 'rows').text = '1'
    cET.SubElement(mean_img, 'cols').text = str(imgSize * imgSize * input_dim_channels)
    cET.SubElement(mean_img, 'dt').text = 'f'
    cET.SubElement(mean_img, 'data').text = ' '.join(
        ['%e' % n for n in np.reshape(data, (imgSize * imgSize * input_dim_channels))])

    tree = cET.ElementTree(root)
    tree.write(fname)
    x = xml.dom.minidom.parse(fname)
    with open(fname, 'w') as f:
        f.write(x.toprettyxml(indent='  '))


srcTrain = os.path.join("..", "training")
srcTest = os.path.join("..", "testing")
xml_file = os.path.join("..", "train_mean.xml")
training = os.path.join("..", "training.txt")
testing = os.path.join("..", "testing.txt")

imgSize = 100


def make_text_files():
    data_mean = np.zeros((input_dim_channels, imgSize, imgSize))
    with open(training, 'w') as file:
        for fileName in os.listdir(srcTrain):
            img = Image.open(os.path.join(srcTrain, fileName))
            img = img.resize((imgSize, imgSize))
            img_data = np.asarray(img, dtype=np.uint16)
            img_data = img_data.reshape((input_dim_channels, imgSize, imgSize))
            data_mean += img_data
            file.write(os.path.join(os.getcwd(), srcTrain, fileName) + "\t")
            count = 0
            for name in names:
                if name in fileName:
                    file.write(str(count) + "\n")
                    break
                else:
                    count += 1
            # file.write("\n")

    data_mean /= len(os.listdir(srcTrain))
    save_mean(xml_file, data_mean)
    data_mean = data_mean.flatten()
    mean = np.sum(data_mean) / (input_dim_channels * imgSize * imgSize)
    max = data_mean[np.argmax(data_mean)]
    min = data_mean[np.argmin(data_mean)]
    print(max)
    print(min)
    print("diff: {}".format(max-min))
    with open(mean_file, 'w') as file:
        file.write(str(mean))

    with open(testing, 'w') as file:
        for fileName in os.listdir(srcTest):
            file.write(os.path.join(os.getcwd(), srcTest, fileName + "\t"))
            count = 0
            for name in names:
                if name in fileName:
                    file.write(str(count) + "\n")
                else:
                    count += 1

    print("JPG's saved to text file")


if __name__ == "__main__":
    make_text_files()
