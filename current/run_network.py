import os
from cntk.ops.functions import load_model
from PIL import Image
import numpy as np
import xml
import time
import noiseMaker
import photoshop
import matplotlib.pyplot as plt

names = os.listdir(os.path.join("..", "pictures"))

model_file = os.path.join("..", "facialRecognition.dnn")
network = load_model(model_file)

test_images = os.path.join("..", "testing")

mean_image_path = os.path.join("..", "train_mean.xml")

image_size = 100
mean_image = xml.etree.ElementTree.parse(mean_image_path).getroot()
mean_image = [float(i) for i in mean_image.find('MeanImg').find('data').text.strip().split(' ')]
mean_image = np.array(mean_image, dtype=np.float32).reshape((image_size, image_size, 3)).transpose((2, 0, 1))


def test_all():
    correct = [0 for _ in range(len(names))]
    wrong = [0 for __ in range(len(names))]
    for photo in os.listdir(test_images):
        rgb_image = np.asarray(Image.open(os.path.join(test_images, photo)), dtype=np.float32)
        bgr_image = rgb_image[..., [2, 1, 0]]
        pic = np.ascontiguousarray(np.rollaxis(bgr_image, 2))
        pic = pic - mean_image
        predictions = np.squeeze(network.eval({network.arguments[0]: [pic]}))
        top_class = np.argmax(predictions)
        top_class = int(top_class)
        if names[top_class] in photo:
            correct[top_class] += 1
        else:
            wrong[top_class] += 1

    print("Correct: ", correct)
    print("Wrong: ", wrong)
    wrong_sum = sum(wrong)
    correct_sum = sum(correct)
    error = (wrong_sum / len(os.listdir(test_images))) * 100
    print("Correct: {0}, Wrong: {1}, Error: {2}%".format(correct_sum, wrong_sum, error))
    return error


def test_image(image_data, name):
    rgb_image = np.asarray(image_data, dtype=np.float32)
    bgr_image = rgb_image[..., [2, 1, 0]]
    pic = np.ascontiguousarray(np.rollaxis(bgr_image, 2))
    pic = pic - mean_image
    predictions = np.squeeze(network.eval({network.arguments[0]: [pic]}))
    top_class = np.argmax(predictions)
    top_class = int(top_class)
    return names[top_class] in name


if __name__ == "__main__":
    # photoshop.run_photoshop()
    data = {"index": [], "error": []}
    for i in range(30):
        correct = 0
        wrong = 0
        testing_dir = os.path.join("..", "testing")
        for image in os.listdir(testing_dir):
            noised_image = noiseMaker.add_black_noise(os.path.join(testing_dir, image), i / 100)
            if test_image(noised_image, image):
                correct += 1
            else:
                wrong += 1
        error = (wrong / len(os.listdir(test_images))) * 100
        data["index"].append(i / 100)
        data["error"].append(error)

    print(data)
    plt.figure(1)
    plt.subplot(211)
    plt.plot(data["index"], data["error"], 'b--')
    plt.xlabel('Noise Percentage')
    plt.ylabel('Error Rate Percentage')
    plt.title('Error rate vs. Noise Increase')

    plt.show()


# what i planned to do
# background (think cs2 level)
# my project
# what i did
# initial work
# stages of work
# problems (categorize w/examples)
# results
# future
