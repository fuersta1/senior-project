# Import the relevant components
from __future__ import print_function
import numpy as np
import os

import cntk as c
import cntk.io.transforms as xforms
from cntk.ops import input_variable, element_times
from cntk import Trainer, learning_rate_schedule, UnitType, CheckpointConfig, CrossValidationConfig
from cntk.io import ImageDeserializer, MinibatchSource, StreamDef, StreamDefs
from cntk.initializer import glorot_uniform
from cntk.layers import default_options, Dense
from cntk.learner import sgd
from cntk.logging import *
import matplotlib.pyplot as plt

# script constants
mean_file = os.path.join("..", "train_mean.xml")
training_file = os.path.join("..", "training.txt")
testing_file = os.path.join("..", "testing.txt")
model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "models")
model_name = "peopleMatcher"

input_dim_x = 100
input_dim_y = 100
# input_dim = input_dim_x * input_dim_y
input_dim_channels = 3
output_classes = len(os.listdir(os.path.join("..", "pictures")))  # 2
print("Num of output classes: {}".format(output_classes))
num_hidden_layers = 1
hidden_layer_dim = 6000


def create_reader(path, is_training, num_label_classes, length=c.io.INFINITELY_REPEAT):
    #  transformation pipeline for the features has jitter/crop only when training
    transforms = []
    if is_training:
        transforms += [
            xforms.crop(crop_type='randomside', side_ratio=0.8)  # train uses data augmentation (translation only)
        ]
    transforms += [
        xforms.scale(width=input_dim_x, height=input_dim_y, channels=input_dim_channels, interpolations='linear'),
        xforms.mean(mean_file)
    ]
    # Read a CTF formatted text (as mentioned above) using the CTF deserializer from a file
    return MinibatchSource(ImageDeserializer(path, StreamDefs(
        labels=StreamDef(field='label', shape=num_label_classes),
        features=StreamDef(field='image', transforms=transforms)
    )),  epoch_size=length)


def create_network(features, hidden_layers):
    global hidden_layer_dim
    with default_options(init=glorot_uniform(), activation=c.ops.relu):
        h = features
        # h = Convolution((5, 5), 32, init=glorot_uniform(), activation=c.ops.relu, pad=True)(h)
        for qwerty in range(hidden_layers):
            h = Dense(hidden_layer_dim)(h)
            hidden_layer_dim = hidden_layer_dim // 2
        r = Dense(output_classes, activation=None)(h)
        return r


def create_train_network():
    c.device.set_default_device(c.device.gpu(0))
    np.random.seed(0)

    if not os.path.isfile(training_file):  # confirm training file exists where it should be
        raise ValueError("Training file not found")
    else:
        num_lines = sum(1 for _ in open(training_file))
        print("Training file found at: {}, Num Objects: {}".format(training_file, num_lines))

    if not os.path.isfile(testing_file):  # confirm testing file exists where it should be
        raise ValueError("Testing file not found")
    else:
        num_lines = sum(1 for _ in open(testing_file))
        print("Testing file found at: {}, Num Objects: {}".format(testing_file, num_lines))

    if not os.path.isfile(mean_file):  # confirm mean file exists where it should be
        raise ValueError("XML Mean file not found")
    else:
        print("XML file found at: {}".format(mean_file))

    # REMOVE THIS WHEN ERROR DURING TRAINING IS FIXED
    if os.path.exists(model_path):  # delete pre-existing model
        for file in os.listdir(model_path):
            os.remove(os.path.join(model_path, file))
        os.rmdir(os.path.join("models"))

    input_layer = input_variable((input_dim_channels, input_dim_x, input_dim_y), name="input_layer")
    label = input_variable(output_classes, name="output_layer")

    # scale input values to between 0-1
    scale = 1.0 / 256.0
    input_var = element_times(scale, input_layer)
    # network = create_network(input_layer / 256, num_hidden_layers)
    network = create_network(input_var, num_hidden_layers)
    # print(input_var)
    loss = c.ops.cross_entropy_with_softmax(network, label)
    label_error = c.ops.classification_error(network, label)

    learning_rate = .2
    learner_schedule = learning_rate_schedule(learning_rate, UnitType.minibatch)
    learner = sgd(network.parameters, learner_schedule)

    trainer = Trainer(network, (loss, label_error), [learner])

    # Initialize the parameters for the trainer
    minibatch_size = 64

    # Create the reader to training data set
    reader_train = create_reader(training_file, True, output_classes)
    reader_test = create_reader(testing_file, False, output_classes, c.io.FULL_DATA_SWEEP)

    # Map the data streams to the input and labels.
    input_map = {
        label: reader_train.streams.labels,
        input_layer: reader_train.streams.features
    }

    # Run the trainer on and perform model training
    training_progress_output_freq = 500

    progress_printer = ProgressPrinter(freq=training_progress_output_freq)
    schedule = c.train.minibatch_size_schedule([(12, 32), (15, 64), (1, 128)], minibatch_size)

    print("Starting training")

    repeats_per_sample = 50
    samples = sum(repeats_per_sample for _ in open(training_file))

    # ts = c.train.training_session(trainer=trainer, mb_source=reader_train, max_samples=samples,
    #                               mb_size=schedule, progress_printer=progress_printer, restore=True,
    #                               progress_frequency=training_progress_output_freq, var_to_stream=input_map,
    #                               checkpoint_config=CheckpointConfig(filename=os.path.join(model_path, model_name)),
    #                               cv_config=CrossValidationConfig(source=reader_test, mb_size=minibatch_size))
    # ts.train()
    samples = samples // 5
    # perform model training
    batch_index = 0
    plot_data = {'batchindex': [], 'loss': [], 'error': []}
    for epoch in range(5):  # loop over epochs
        sample_count = 0
        while sample_count < samples:  # loop over minibatches in the epoch
            data = reader_train.next_minibatch(min(minibatch_size, samples - sample_count),
                                               input_map=input_map)  # fetch minibatch.
            trainer.train_minibatch(data)  # update model with it

            sample_count += data[input_layer].num_samples  # count samples processed so far
            # For visualization...
            plot_data['batchindex'].append(batch_index)
            plot_data['loss'].append(trainer.previous_minibatch_loss_average)
            plot_data['error'].append(trainer.previous_minibatch_evaluation_average * 100)
            print("[Epoch] [{0} out of 5], [Sample] [{1} out of {2}][Training] loss = {3:.3f}, metric = {4:.3f}%".format
                  (epoch, sample_count, samples, trainer.previous_minibatch_loss_average,
                   trainer.previous_minibatch_evaluation_average * 100))
            batch_index += 1
            if trainer.previous_minibatch_loss_average < 0.0001:
                print("skipping trsining")
                #break
        trainer.summarize_training_progress()
    plt.figure(1)
    plt.subplot(211)
    plt.plot(plot_data["batchindex"], plot_data["loss"], 'b--')
    plt.xlabel('Minibatch number')
    plt.ylabel('Loss')
    plt.title('Minibatch run vs. Training loss ')

    plt.show()

    plt.subplot(212)
    plt.plot(plot_data["batchindex"], plot_data["error"], 'r--')
    plt.xlabel('Minibatch number')
    plt.ylabel('Label Prediction Error')
    plt.title('Minibatch run vs. Label Prediction Error ')
    plt.show()
    print("stopped training")

    # process minibatches and evaluate the model
    metric_numer = 0
    metric_denom = 0
    minibatch_index = 0

    # testing network
    epoch_size = sum(2 for _ in open(testing_file))
    reader_test = create_reader(testing_file, False, output_classes, epoch_size)
    minibatch_size = 16
    sample_count = 0
    while sample_count < epoch_size:
        curr_batch = min(minibatch_size, epoch_size-sample_count)
        data = reader_test.next_minibatch(curr_batch, input_map=input_map)
        trainer.test_minibatch(data)

        # minibatch data to be trained with
        metric_numer += trainer.test_minibatch(data) * curr_batch
        metric_denom += curr_batch

        # Keep track of the number of samples processed so far.
        sample_count += data[input_layer].num_samples
        minibatch_index += 1

    performance = (metric_numer*100.0)/metric_denom
    if performance >= 45.0:
        raise ValueError("Testing results too low for use, errs = {:0.1f}".format(performance))

    print("Testing complete")
    print("Final Results: Minibatch[1-{}]: errs = {:0.1f}% * {}".format(minibatch_index+1,
                                                                        performance, metric_denom))
    print("")

    network.set_name("facial_recognition")
    save_file = os.path.join("..", "facialRecognition.dnn")
    if os.path.isfile(save_file):
        os.remove(save_file)
    network.save_model(save_file)


if __name__ == "__main__":
    create_train_network()
