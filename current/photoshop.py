from PIL import Image
import os.path

def_size = 100


def is_white(pixel):
    # returns True is a pixel is "white"
    if type(pixel) == int:
        return False
    for pix in pixel:
        if pix < 240:
            return False
    return True


def is_green(pixel):
    # returns True if a pixel is "green"
    if type(pixel) == int:
        return False
    if 74 < pixel[0] < 76 and pixel[1] > 250 and pixel[2] < 5:
        return True
    return False


# photoshops a picture at src_image with that at _photoshop_image
# returns a PIL Image
def photoshop(src_image_path, photoshop_image_path):
    reference_image = Image.open(photoshop_image_path, mode='r')
    reference_image = reference_image.resize((def_size, def_size))
    source_image = Image.open(src_image_path, mode='r')
    source_image = source_image.resize((def_size, def_size))
    for i in range(def_size):
        for q in range(def_size):
            if "apple" in src_image_path or "orange" in src_image_path:
                if is_white(source_image.getpixel((i, q))):
                    source_image.putpixel((i, q), reference_image.getpixel((i, q)))
            else:
                if is_green(source_image.getpixel((i, q))):
                    source_image.putpixel((i, q), reference_image.getpixel((i, q)))
    return source_image


# computes the new file name for a photoshoped image
def new_file_name(src_image, photoshop_image, save_dir):
    src_name = src_image.split("\\")[4]
    photoshop_name = photoshop_image.split(".")[2]
    photoshop_name = photoshop_name.split("\\")[2]
    return os.path.join(save_dir, photoshop_name + src_name)


def run_photoshop():

    source_dirs = os.listdir(os.path.join("..", "pictures"))
    print(source_dirs)
    training_dir = os.path.join("..", "training")
    testing_dir = os.path.join("..", "testing")
    photoshop_pics = os.path.join("..", "photoshop_pics")

    for file in os.listdir(training_dir):
        os.remove(os.path.join(training_dir, file))
        print(file, " deleted")

    for file in os.listdir(testing_dir):
        os.remove(os.path.join(testing_dir, file))
        print(file, " deleted")

    # photoshop everything in training sub-directories and save to ..\training\
    for directory in source_dirs:
        directory = os.path.join("..", "pictures", directory, "training")
        print(directory)
        for file in os.listdir(directory):
            for pic in os.listdir(photoshop_pics):
                name = new_file_name(os.path.join(directory, file), os.path.join(photoshop_pics, pic), training_dir)
                new_photo = photoshop(os.path.join(directory, file), os.path.join(photoshop_pics, pic))
                print(name, " saved")
                new_photo.convert('RGB').save(name)

    # photoshop everything in testing sub-directories and save to ..\testing\
    for directory in source_dirs:
        directory = os.path.join("..", "pictures", directory, "testing")
        for file in os.listdir(directory):
            for pic in os.listdir(photoshop_pics):
                name = new_file_name(os.path.join(directory, file), os.path.join(photoshop_pics, pic), testing_dir)
                new_photo = photoshop(os.path.join(directory, file), os.path.join(photoshop_pics, pic))
                print(name, " saved")
                new_photo.convert('RGB').save(name)

    print("Photoshopping Done")

if __name__ == '__main__':
    run_photoshop()
